extends Control
export (int) var Day
export (int) var Bonus
var Checked=false
func _setChecked(value):
	Checked=value
	if(value):
		$Daily.call_deferred("show")
	else:
		$Daily.call_deferred("hide")
