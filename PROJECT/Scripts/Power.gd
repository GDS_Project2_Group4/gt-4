extends KinematicBody2D

export (int) var min_speed # Minimum speed range.
export (int) var max_speed # Maximum speed range.

var moveVector
var speed
func _on_Visible_screen_exited():
	queue_free()

func _physics_process(delta):
	self.position+=moveVector*speed*delta