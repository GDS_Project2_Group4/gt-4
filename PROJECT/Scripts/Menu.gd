extends CanvasLayer

#MENU
export (int) var Respawn=0
export (int) var MapVariant=1
export (int) var PlayerVariant=1
export (int) var ControlVariant=1
export (int) var VibrateVariant=0
export (int) var Sound=45
export (int) var Music=45

var Points=0
var Mapa1=1
var Mapa2=0
var Mapa3=0
var LastTime=-1

var Maps
var Players
var Controls
var Vibrates
var Days
var Plaing=false
func _ready():
	get_tree().set_auto_accept_quit(false)
	Players=self.get_tree().get_nodes_in_group("PlayerVariant")
	Maps=self.get_tree().get_nodes_in_group("MapVariant")
	Controls=self.get_tree().get_nodes_in_group("ControlVariant")
	Vibrates=self.get_tree().get_nodes_in_group("VibrationVariant")
	Days=self.get_tree().get_nodes_in_group("Day")
	load_config()
	setProperty()
	$Menu.play()
	get_tree().paused=true

func _physics_process(delta):
	if(Plaing):
		Plaing=false
		get_tree().change_scene("res://Scenes/HexagonLine.tscn")

func _BackOrExit():
	if($Main/Customize.visible):
		_on_Back_Customize_pressed()
		return
	if($Main/Setting.visible):
		_on_Back_Setting_pressed()
		return
	if($Main/Control.visible):
		_on_Back_Control_pressed()
		return
	if($Main/Player.visible):
		_on_Back_Player_pressed()
		return
	if($Main/Map.visible):
		_on_Back_Map_pressed()
		return
	if($Main/Audio.visible):
		_on_Back_Audio_pressed()
		return
	if($Main/About.visible):
		_on_Back_About_pressed()
		return
	if($Main/How.visible):
		_on_Back_How_pressed()
		return
	if($Main/Daily.visible):
		_on_Back_Daily_pressed()
		return
	
	get_tree().quit()

func _on_Play_pressed():
	$Main/Menu.call_deferred("hide")
	$Main/Loading.call_deferred("show")
	$Click.play()

func _input(event):
	var cancel =  event.is_action("ui_cancel") and event.is_pressed()
	
	if(cancel):
		_BackOrExit()

func _on_Customize_pressed():
	$Main/Menu.call_deferred("hide")
	$Main/Customize.call_deferred("show")
	$Click.play()


func _on_Back_Customize_pressed():
	$Main/Menu.call_deferred("show")
	$Main/Customize.call_deferred("hide")
	$Click.play()


func _on_Setting_pressed():
	$Main/Menu.call_deferred("hide")
	$Main/Setting.call_deferred("show")
	$Click.play()


func _on_Back_Setting_pressed():
	$Main/Menu.call_deferred("show")
	$Main/Setting.call_deferred("hide")
	$Click.play()


func _on_Back_Control_pressed():
	$Main/Control.call_deferred("hide")
	$Main/Setting.call_deferred("show")
	$Click.play()


func _on_Control_pressed():
	$Main/Setting.call_deferred("hide")
	$Main/Control.call_deferred("show")
	$Click.play()


func _on_Back_Player_pressed():
	$Main/Player.call_deferred("hide")
	$Main/Customize.call_deferred("show")
	$Click.play()


func _on_Player_pressed():
	$Main/Customize.call_deferred("hide")
	$Main/Player.call_deferred("show")
	$Click.play()


func _on_Map_pressed():
	$Main/Customize.call_deferred("hide")
	$Main/Map.call_deferred("show")
	$Click.play()

func _on_Back_Map_pressed():
	$Main/Map.call_deferred("hide")
	$Main/Customize.call_deferred("show")
	$Click.play()

func _on_Back_Daily_pressed():
	$Main/Daily.call_deferred("hide")
	$Main/Menu.call_deferred("show")
	$Click.play()

func _on_About_pressed():
	$Main/Menu.call_deferred("hide")
	$Main/About.call_deferred("show")
	$Click.play()

func _on_Back_About_pressed():
	$Main/Menu.call_deferred("show")
	$Main/About.call_deferred("hide")
	$Click.play()

func _on_How_pressed():
	$Main/Menu.call_deferred("hide")
	$Main/How.call_deferred("show")
	$Click.play()

func _on_Back_How_pressed():
	$Main/Menu.call_deferred("show")
	$Main/How.call_deferred("hide")
	$Click.play()

func _on_Audio_pressed():
	$Main/Setting.call_deferred("hide")
	$Main/Audio.call_deferred("show")
	$Click.play()

func _on_Back_Audio_pressed():
	$Main/Audio.call_deferred("hide")
	$Main/Setting.call_deferred("show")
	$Click.play()

func _notification(what):
    if what == MainLoop.NOTIFICATION_WM_QUIT_REQUEST:
        _BackOrExit()
    elif what == MainLoop.NOTIFICATION_WM_GO_BACK_REQUEST:
        _BackOrExit()

func save_config(): 
	var path = "user://config.ini" # The path to save the file
	var config = ConfigFile.new() # Create a new ConfigFile object 

###########MENU###################
	config.set_value("control", "variant", ControlVariant)
	config.set_value("audio", "sound", Sound)
	config.set_value("audio", "music", Music)
	config.set_value("audio", "vibrateVariant", VibrateVariant)
	config.set_value("mapa", "variant", MapVariant)
	config.set_value("mapa", "1", Mapa1)
	config.set_value("mapa", "2", Mapa2)
	config.set_value("mapa", "3", Mapa3)
	config.set_value("player", "variant", PlayerVariant)
	for player in Players:
		if(player.Enable):
			config.set_value("player", str(player.Index), 1)
		else:
			config.set_value("player", str(player.Index), 0)
	
	var err = config.save(path) # Save the configuration file to the disk 
	if err != OK: # If there's an error 
		print("Some error occurred")
	
	path = "user://points.ini" # The path to save the file
	config = ConfigFile.new() # Create a new ConfigFile object
	config.set_value("points", "value", Points)
	config.set_value("player", "respawn", Respawn)
	err = config.save(path) # Save the configuration file to the disk 
	if err != OK: # If there's an error 
		print("Some error occurred")


func save_config_day(): 
	var path = "user://daily.ini" # The path to save the file
	var config = ConfigFile.new() # Create a new ConfigFile object 
	for day in Days:
		if(day.Checked):
			config.set_value("day", str(day.Day), 1)
	config.set_value("time", "unit", LastTime)
	var err = config.save(path) # Save the configuration file to the disk 
	if err != OK: # If there's an error 
		print("Some error occurred")

func setProperty():
	#MAPA
	$Main/Map/Map/Map1.Enable=Mapa1==1
	$Main/Map/Map/Map2.Enable=Mapa2==1
	$Main/Map/Map/Map3.Enable=Mapa3==1
	for map in Maps:
		if(map.Index==MapVariant):
			map._setChecked(true)
		if(map.Enable):
			map._Price(0)

	#PLAYER
	for player in Players:
		if(player.Index==PlayerVariant):
			player._setChecked(true)
		if(player.Enable):
			player._Price(0)
	
	#CONTROL
	for control in Controls:
		if(control.Index==ControlVariant):
			control._setChecked(true)
	
	#AUDIO
	for vibrate in Vibrates:
		if(vibrate.Index==VibrateVariant):
			vibrate._setChecked(true)

	$Main/Audio/Audio/Sound.value=Sound
	$Main/Audio/Audio/Music.value=Music
	AudioServer.set_bus_volume_db(AudioServer.get_bus_index("Music"),-60+Music)
	AudioServer.set_bus_volume_db(AudioServer.get_bus_index("Sound"),-60+Sound)

	#DAILY

	if(LastTime==-1):
		activeDailyBonus()
	elif((OS.get_unix_time()-LastTime>=(60*60*24) && OS.get_unix_time()-LastTime<=(60*60*24*2))):
		activeDailyBonus()
	elif(OS.get_unix_time()-LastTime>(60*60*24*2)):
		for day in Days:
				if(day.Day>1):
					day._setChecked(false)
				else:
					Points=Points+day.Bonus
		LastTime=OS.get_unix_time()
		save_config_day()
		$Main/Daily.call_deferred("show")
		$Main/Menu.call_deferred("hide")

	#POINTS
	_refreshPoints()

func activeDailyBonus():
		var allDay=true
		for day in Days:
			if(!day.Checked):
				day._setChecked(true)
				allDay=false
				if(day.Bonus>0):
					Points=Points+day.Bonus
				else:
					Respawn=1
				save_config()
				break
		if(allDay):
			for day in Days:
				if(day.Day>1):
					day._setChecked(false)
				else:
					Points=Points+day.Bonus
		LastTime=OS.get_unix_time()
		save_config_day()
		$Main/Daily.call_deferred("show")
		$Main/Menu.call_deferred("hide")

func _refreshPoints():
	$Main/Map/Map/Point.text=str(Points)
	$Main/Player/Player/Point.text=str(Points)
	$Main/Customize/Customize/Point.text=str(Points)

func load_config(): 
	var path = "user://config.ini" # The path to load the file 
	var config = ConfigFile.new() # Create a new ConfigFile object 
	var err = config.load(path) 
	# Load the file from the disk 
	if err == OK:  
		##MENU
		var controlVariant=int(config.get_value("control", "variant", ControlVariant))
		if(controlVariant>0):
			ControlVariant = controlVariant
		
		var sound=int(config.get_value("audio", "sound", Sound))
		if(sound>=0):
			Sound = sound
		
		var music=int(config.get_value("audio", "music", Music))
		if(music>=0):
			Music = music
		
		var vibrateVariant=int(config.get_value("audio", "vibrateVariant", VibrateVariant))
		if(vibrateVariant>=0):
			VibrateVariant = vibrateVariant
		
		var mapVariant=int(config.get_value("mapa", "variant", MapVariant))
		if(mapVariant>0):
			MapVariant = mapVariant
		
		var mapa1=int(config.get_value("mapa", "1", Mapa1))
		if(mapa1>0):
			Mapa1 = mapa1
		
		var mapa2=int(config.get_value("mapa", "2", Mapa2))
		if(mapa2>0):
			Mapa2 = mapa2
		
		var mapa3=int(config.get_value("mapa", "3", Mapa3))
		if(mapa3>0):
			Mapa3 = mapa3
		
		var playerVariant=int(config.get_value("player", "variant", PlayerVariant))
		if(playerVariant>0):
			PlayerVariant = playerVariant
		for player in Players:
			if(!player.Enable):
				var value=int(config.get_value("player", str(player.Index), 0))
				if(value>0):
					player.Enable = true
			
	path = "user://points.ini" # The path to load the file 
	config = ConfigFile.new() # Create a new ConfigFile object 
	err = config.load(path) 
	# Load the file from the disk 
	if err == OK:
		#Points
		var points=int(config.get_value("points", "value", Points))
		if(points>0):
			Points = points
		
		#RESPAWN
		var respawn=float(config.get_value("player", "respawn", Respawn))
		if(respawn>0):
			Respawn = respawn
	
	path = "user://daily.ini" # The path to load the file 
	config = ConfigFile.new() # Create a new ConfigFile object 
	err = config.load(path) 
	# Load the file from the disk 
	if err == OK:
		#Points
		for day in Days:
				var active=int(config.get_value("day", str(day.Day), 0))
				if(active>0):
					day._setChecked(true)
		#TIME
		var time=int(config.get_value("time", "unit", LastTime))
		if(time>-1):
			LastTime=time

func _on_Map_Select(Index):
	for map in Maps:
		if(map.Index==Index):
			map._setChecked(true)
			MapVariant=Index
			save_config()
		else:
			map._setChecked(false)
	$Click.play()

func _on_Buy(Index):
	for map in Maps:
		if(map.Index==Index):
			if(map.Points<=Points):
				$Main/Map/Unlock/Unlock.Index=Index
				_OpenUnlockMap()
				$Click.play()
				break

func _OpenUnlockMap():
	self.pause_mode=PAUSE_MODE_STOP
	$Main/Map/Map/Back.call_deferred("hide")
	$Main/Map/Map/Map1.EnableClick(false)
	$Main/Map/Map/Map2.EnableClick(false)
	$Main/Map/Map/Map3.EnableClick(false)
	$Main/Map/Unlock.call_deferred("show")

func _CloseUnlockMap():
	self.pause_mode=PAUSE_MODE_PROCESS
	$Main/Map/Map/Back.call_deferred("show")
	$Main/Map/Map/Map1.EnableClick(true)
	$Main/Map/Map/Map2.EnableClick(true)
	$Main/Map/Map/Map3.EnableClick(true)
	$Main/Map/Unlock.call_deferred("hide")

func _OpenUnlockPlayer():
	self.pause_mode=PAUSE_MODE_STOP
	$Main/Player/Player/Back.call_deferred("hide")
	$Main/Player/Player/Player1.EnableClick(false)
	$Main/Player/Player/Player2.EnableClick(false)
	$Main/Player/Player/Player3.EnableClick(false)
	$Main/Player/Player/Player4.EnableClick(false)
	$Main/Player/Player/Player5.EnableClick(false)
	$Main/Player/Unlock.call_deferred("show")

func _CloseUnlockPlayer():
	self.pause_mode=PAUSE_MODE_PROCESS
	$Main/Player/Player/Back.call_deferred("show")
	$Main/Player/Player/Player1.EnableClick(true)
	$Main/Player/Player/Player2.EnableClick(true)
	$Main/Player/Player/Player3.EnableClick(true)
	$Main/Player/Player/Player4.EnableClick(true)
	$Main/Player/Player/Player5.EnableClick(true)
	$Main/Player/Unlock.call_deferred("hide")

func _on_Unlock_No(Index):
	_CloseUnlockMap()
	$Click.play()

func _SetMapToSave():
	if($Main/Map/Map/Map1.Enable):
		Mapa1=1
	if($Main/Map/Map/Map2.Enable):
		Mapa2=1
	if($Main/Map/Map/Map3.Enable):
		Mapa3=1

func _on_Unlock_Yes(Index):
	for map in Maps:
		if(map.Index==Index):
			Points=Points-map.Points
			_refreshPoints()
			map._setEnable(true)
			map._Price(0)
			_SetMapToSave()
			map._Buy()
			break
	_CloseUnlockMap()
	$Click.play()


func _on_UnlockPlayer_No(Index):
	_CloseUnlockPlayer()
	$Click.play()

func _on_UnlockPlayer_Yes(Index):
	for player in Players:
		if(player.Index==Index):
			Points=Points-player.Points
			_refreshPoints()
			player._setEnable(true)
			player._Price(0)
			player._Buy()
			break
	_CloseUnlockPlayer()
	$Click.play()

func _on_Player_Buy(Index):
	for player in Players:
		if(player.Index==Index):
			if(player.Points<=Points):
				$Main/Player/Unlock/UnlockPlayer.Index=Index
				_OpenUnlockPlayer()
				$Click.play()
				break

func _on_Player_Select(Index):
	for player in Players:
		if(player.Index==Index):
			player._setChecked(true)
			PlayerVariant=Index
			save_config()
		else:
			player._setChecked(false)
	$Click.play()

func _on_ActivationControl_Select(Index):
	for control in Controls:
		if(control.Index==Index):
			control._setChecked(true)
			ControlVariant=Index
			save_config()
		else:
			control._setChecked(false)
	$Click.play()


func _on_Sound_value_changed(value):
	Sound=int(value)
	AudioServer.set_bus_volume_db(AudioServer.get_bus_index("Sound"),-60+Sound)
	save_config()


func _on_Music_value_changed(value):
	Music=int(value)
	AudioServer.set_bus_volume_db(AudioServer.get_bus_index("Music"),-60+Music)
	save_config()

func _on_ActivationVibrate_Select(Index):
	for vibrate in Vibrates:
		if(vibrate.Index==Index):
			vibrate._setChecked(true)
			VibrateVariant=Index
			save_config()
		else:
			vibrate._setChecked(false)
	$Click.play()

func _on_Loading_draw():
	Plaing=true
