extends KinematicBody2D
signal hit
signal speedPowerUps(body)
signal bombPowerUps(body)
signal eatPowerUps(body)
var disable = false
var pointPackman=1
var isHexman=false
func _ready():
	disablePlayer()

func _physics_process(delta):
	if($Shield.visible):
	 $Shield.rotate(delta)

func respown():
	defaultState()
	start()
	self.show()

func _on_Detect_body_entered(body):
	if(body.is_in_group("PowerUps")):
		if(body.is_in_group("Eat")):
			isHexman=true
			emit_signal("eatPowerUps",body)
		if(body.is_in_group("Speed")):
			emit_signal("speedPowerUps",body)
		if(body.is_in_group("Bomb")):
			emit_signal("bombPowerUps",body)
	elif(body.is_in_group("Mob")):
		if(isEatState() || $Sprite.animation=="eat-default"):
			self.get_tree().root.get_child(0).AddSocer(pointPackman)
			body.get_parent().destoryed()
		else:
			disablePlayer()
			$Dead.play()
			$Sprite.play("destroyed")
			get_tree().paused=true
	

func eatState():
	isHexman=true
	$Shield.visible=true
	$Sprite.animation="eat"

func setFrames(player):
	$Sprite.frames=player.get_child(0).frames
	
func defaultState():
	isHexman=false
	$Shield.visible=false
	$Sprite.animation="default"
	
func isEatState():
	return isHexman
	
func eatDefaultState():
	isHexman=true
	$Shield.visible=true
	$Sprite.animation="eat-default"
	$AnimationPlayer.play("Shield")

func start():
	#show()
	$CollisionShape2D.disabled=false
	$Detect/CollisionShape2D.disabled=false
	disable = false
	$Sprite.playing=true

func disablePlayer():
		#hide()
		$CollisionShape2D.disabled=true
		$Detect/CollisionShape2D.disabled=true
		disable = true

func _on_Sprite_animation_finished():
	if($Sprite.animation=="destroyed"):
		self.hide()
		emit_signal("hit")


func _on_AnimationPlayer_animation_finished(anim_name):
	if(anim_name=="Shield"):
		defaultState()
