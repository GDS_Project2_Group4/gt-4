extends TextureRect
signal Yes(Index)
signal No(Index)

var Index=0

func _on_Unlock_pressed():
	emit_signal("Yes",Index)

func _on_Cancel_pressed():
	emit_signal("No",Index)
