extends Area2D

var Sfera
#var Player
signal DownChangeSfera(down,body)
signal ChangeSfera(body)
func _ready():
	#Player=self.get_tree().get_nodes_in_group("Player")[0]
	Sfera=self.get_parent()

func _on_DownChangeSfera_body_entered(body):
	if(body.get_parent().get_parent().is_in_group("down")):
		return
	elif(body.get_parent().get_parent().is_in_group("up")):
		get_tree().root.get_node("HexagonLine").call_deferred ("_on_DownChangeSfera_ChangeSfera",body)
		return
	elif(body.get_parent().bppos==1 && body.get_parent().get_parent().is_in_group("MobSferaHexagon") && body.get_parent().Index>get_tree().root.get_node("HexagonLine").Player.get_parent().Index):
		get_tree().root.get_node("HexagonLine").call_deferred ("_on_DownChangeSfera_DownChangeSfera",Sfera,body)
		return
	return

func _on_DownChangeSfera_body_exited(body):
	if(body.get_parent().bppos==2 && body.get_parent().get_parent().is_in_group("MobSferaHexagon")):
		body.get_parent().bppos=1
	return

